console.log("Hello World");

//arithmetic operations

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);
	 let difference = x - y;
	 console.log("Result of subtraction operator: " + difference);
	 let product = x * y;
	 console.log("Result of multiplication operator: " + product);
	 let qoutient = x / y;
	 console.log("Result of division operator: " + qoutient);
	 let remainder = y % x;
	 console.log("Result of modulo operator: " + remainder);

//assignment operator

	//basicassignment operator
	let assignmentNumber = 8;

	//addition assignment operator
	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	//subtraction/multiplivcaton/division assignment operator
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);


	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);


	//multiple operators and parenthetes

	/*
	PEMDAS will be applied if multiple operation
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS opperation: " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of MDAS opperation: " + pemdas);

	//increment/decrement
	//operators that add or subtract values by 1 and reassign the values of the variable before the increment or decrement was applied

	//pre-increment
	let z = 1;
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	//post-increment

	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);


	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z --;
	console.log("Result of post-drecement: " + decrement);
	console.log("Result of post-decrement: " + z)

	//type coercion

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);


	// let nonCoercion = numC + numD;
	// console.log(conCoercion);
	// console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false +1;
	console.log(numF);


	//compareson operators

	let juan = 'juan';
	//equality operator

	console.log (1 == 1);
	console.log (1 == 2);
	console.log (1 == '1');
	console.log (0 == false);
	console.log ('juan' == 'juan');
	console.log ('juan' == juan);


	//enequality operator


	console.log (1 != 1);
	console.log (1 != 2);
	console.log (1 != '1');
	console.log (0 != false);
	console.log ('jua!' != 'juan');
	console.log ('juan' != juan);

	//strict equality operator
	console.log (1 === 1);
	console.log (1 === 2);
	console.log (1 === '1');
	console.log (0 === false);
	console.log ('juan' === 'juan');
	console.log ('juan' === juan);

	//relational operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a<= b;

	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr);
	console.log(b <= numStr);

	let str ="tweenty";
	console.log(b >= str);

	//logical operator
	let isLegalAge = true;
	let isRegistered = false;

	let allRequirementsMents = isLegalAge && isRegistered;
	console.log('Result of logical and operator: ' + allRequirementsMents);

	//logical Or Operator ||

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical or operator: " + someRequirementsMet);


	//logical not operator !

	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical not operator: " + someRequirementsNotMet);

	//typeof - check what kind of data type